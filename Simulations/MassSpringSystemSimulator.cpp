#include "MassSpringSystemSimulator.h"


MassSpringSystemSimulator::MassSpringSystemSimulator()
{
	setIntegrator(EULER);
	m_iNumPoints = 0;
	m_iNumSprings = 0;
	m_pSprings = 0;
	m_pPoints = 0;
	m_bCollision = false;
	m_bGravity = false;
}
const char* MassSpringSystemSimulator::getTestCasesStr()
{
	return "Demo 2 (Euler),Demo 3 (Midpoint),Demo 4,Ball";
}
void MassSpringSystemSimulator::initUI(DrawingUtilitiesClass * DUC)
{
	this->DUC = DUC;
	switch (m_iTestCase)
	{
	case 0:
	case 1:
		m_bCollision = false;
		m_bGravity = false;
		m_iIntegratorGUI = 0;
		break;
	case 2:
	case 3:
	{
		TwAddVarRW(DUC->g_pTweakBar, "Gravity", TW_TYPE_BOOLCPP, &m_bGravity, "");
		TwAddVarRW(DUC->g_pTweakBar, "Wall collision", TW_TYPE_BOOLCPP, &m_bCollision, "");
		TwType TW_TYPE_TESTCASE = TwDefineEnumFromString("Integrator", "Euler,Midpoint");
		TwAddVarRW(DUC->g_pTweakBar, "Integrator", TW_TYPE_TESTCASE, &m_iIntegratorGUI, "");
		break;
	}
	default:break;
	}
}
void MassSpringSystemSimulator::reset()
{
	m_mouse.x = m_mouse.y = 0;
	m_trackmouse.x = m_trackmouse.y = 0;
	m_oldtrackmouse.x = m_oldtrackmouse.y = 0;
}
void MassSpringSystemSimulator::drawFrame(ID3D11DeviceContext* pd3dImmediateContext)
{
	for (int i = 0; i < m_iNumPoints; i++)
	{
		Vec3 col = m_pPoints[i].fixed ? Vec3(1.f, 0.f, 0.f) : Vec3(0.f, 1.f, 0.f);
		DUC->setUpLighting(Vec3(), 0.4*Vec3(1, 1, 1), 100, 0.6*col);
		DUC->drawSphere(m_pPoints[i].pos, Vec3(SPHERE_RADIUS, SPHERE_RADIUS, SPHERE_RADIUS));
	}
	DUC->beginLine();
	for (int i = 0; i < m_iNumSprings; i++)
	{
		Vec3 col1 = Vec3(0.f, 1.f, 0.f);
		Vec3 col2 = Vec3(0.f, 0.f, 1.f);
		MPoint* p1 = &m_pPoints[m_pSprings[i].s1];
		MPoint* p2 = &m_pPoints[m_pSprings[i].s2];
		DUC->drawLine(p1->pos, col1, p2->pos, col2);
	}
	DUC->endLine();
}
void MassSpringSystemSimulator::notifyCaseChanged(int testCase)
{
	m_bFirstPrint = true;
	if (m_pPoints)
	{
		delete[] m_pPoints;
		m_pPoints = 0;
	}
	if (m_pSprings)
	{
		delete[] m_pSprings;
		m_pSprings = 0;
	}
	m_externalForceMouse = Vec3(0, 0, 0);
	m_iNumPoints = 0;
	m_iNumSprings = 0;
	m_pSprings = 0;
	m_pPoints = 0;
	setMass(10.0f);
	setDampingFactor(0.0f);
	setStiffness(40.0f);
	applyExternalForce(Vec3(0, 0, 0));
	switch (testCase)
	{
	case 0:
	{
		m_iIntegrator = EULER;
		int p0 = addMassPoint(Vec3(0.0, 0.0f, 0), Vec3(-1.0, 0.0f, 0), false);
		int p1 = addMassPoint(Vec3(0.0, 2.0f, 0), Vec3(1.0, 0.0f, 0), false);
		addSpring(p0, p1, 1.0);
		break;
	}
	case 1:
	{
		m_iIntegrator = MIDPOINT;
		int p0 = addMassPoint(Vec3(0.0, 0.0f, 0), Vec3(-1.0, 0.0f, 0), false);
		int p1 = addMassPoint(Vec3(0.0, 2.0f, 0), Vec3(1.0, 0.0f, 0), false);
		addSpring(p0, p1, 1.0);
		break;
	}
	case 2:
	{
		int points[27];
		float ah = .25f;
		for (int i = 0; i < 27; i++)
		{
			float x = ah - ah*(float)(i % 3);
			float y = ah - ah*(float)((i / 3) % 3);
			float z = ah - ah*(float)(i / 9);
			float vx = 0.0f;
			float vy = 0.0f;
			float vz = 0.0f;
			points[i] = addMassPoint(Vec3(x, y, z), Vec3(vx, vy, vz), false);
		}
		for (int i = 0; i < 18; i++)
		{
			addSpring(i, i + 9, ah);
		}
		for (int i = 0; i < 6; i++)
		{
			addSpring(i, i + 3, ah);
			addSpring(i + 9, i + 12, ah);
			addSpring(i + 18, i + 21, ah);
		}
		for (int i = 0; i < 27; i += 3)
		{
			addSpring(i, i + 1, ah);
			addSpring(i + 1, i + 2, ah);
		}
		m_pPoints[13].vel = Vec3(0.1, 0, 0);
		break;
	}
	case 3:
		addMassPoint(Vec3(0, 0, 0), Vec3(0, 0, 0), false);
		break;
	default:
		cout << "Test-Case #" << testCase << " not implemented";
		break;
	}
	m_iTestCase = testCase;
}
void MassSpringSystemSimulator::externalForcesCalculations(float timeElapsed)
{
	Point2D mouseDiff;
	mouseDiff.x = m_trackmouse.x - m_oldtrackmouse.x;
	mouseDiff.y = m_trackmouse.y - m_oldtrackmouse.y;
	if (mouseDiff.x != 0 || mouseDiff.y != 0)
	{
		Mat4 worldViewInv = Mat4(DUC->g_camera.GetWorldMatrix() * DUC->g_camera.GetViewMatrix());
		worldViewInv = worldViewInv.inverse();
		Vec3 inputView = Vec3((float)mouseDiff.x, (float)-mouseDiff.y, 0);
		Vec3 inputWorld = worldViewInv.transformVectorNormal(inputView);
		// find a proper scale!
		float inputScale = 0.001f;
		inputWorld = inputWorld * inputScale;
		m_externalForceMouse = m_externalForceMouse + inputWorld;
		cout << "Update external force:" << m_externalForceMouse << endl;
	}
	else {
		//m_vfMovableObjectFinalPos = m_vfMovableObjectPos;
	}
}
float VecLen(Vec3 v)
{
	return sqrt(v.x * v.x + v.y * v.y + v.z * v.z);
}
Vec3 Normalize(Vec3 v)
{
	return v / VecLen(v);
}
bool BallInBox(Vec3 x, float r, Vec3 min, Vec3 max)
{
	return x.x >= min.x + r && x.x <= max.x - r &&
		x.y >= min.y + r && x.y <= max.y - r &&
		x.z >= min.z + r && x.z <= max.z - r;
}
void MassSpringSystemSimulator::simulateTimestep(float timeStep)
{
	if (m_iTestCase >= 2)
	{
		if (m_iIntegratorGUI == 0)
			setIntegrator(EULER);
		if (m_iIntegratorGUI == 1)
			setIntegrator(MIDPOINT);
	}
	for (int i = 0; i < m_iNumPoints; i++)
	{
		MPoint* p = &m_pPoints[i];
		p->force = m_externalForce + m_externalForceMouse;
		if (m_bGravity)
			p->force += Vec3(0.f, -9.81f, 0.f);
	}
	if (m_iIntegrator == EULER)
	{
		for (int i = 0; i < m_iNumSprings; i++)
		{
			MSpring* s = &m_pSprings[i];
			MPoint* p1 = &m_pPoints[s->s1];
			MPoint* p2 = &m_pPoints[s->s2];
			float factor = 0.f;
			if (p1->fixed)
				factor = 2.f;
			else if (p2->fixed)
				factor = 2.f;
			else
				factor = 1.f;
			Vec3 diff = p1->pos - p2->pos;
			Vec3 force = Normalize(diff) * m_fStiffness * (VecLen(diff) - s->len) * factor;
			p1->force -= force;
			p2->force += force;
		}
		for (int i = 0; i < m_iNumPoints; i++)
		{
			MPoint* p = &m_pPoints[i];
			Vec3 accel = p->force / m_fMass;
			if (!p->fixed)
			{
				p->pos += p->vel * timeStep;
				p->vel += accel * timeStep;
			}
		}
	}
	else if (m_iIntegrator == MIDPOINT)
	{
		for (int i = 0; i < m_iNumSprings; i++)
		{
			MSpring* s = &m_pSprings[i];
			MPoint* p1 = &m_pPoints[s->s1];
			MPoint* p2 = &m_pPoints[s->s2];
			float factor = 0.f;
			if (p1->fixed)
				factor = 2.f;
			else if (p2->fixed)
				factor = 2.f;
			else
				factor = 1.f;
			Vec3 tv1 = p1->vel + timeStep / 2.f * p1->oldforce / m_fMass;
			Vec3 tx1 = p1->pos + timeStep / 2.f * tv1;
			Vec3 tv2 = p2->vel + timeStep / 2.f * p2->oldforce / m_fMass;
			Vec3 tx2 = p2->pos + timeStep / 2.f * tv2;
			Vec3 diff = tx1 - tx2;
			Vec3 force = Normalize(diff) * m_fStiffness * (VecLen(diff) - s->len) * factor;
			p1->force -= force;
			p2->force += force;
		}
		for (int i = 0; i < m_iNumPoints; i++)
		{
			MPoint* p = &m_pPoints[i];
			Vec3 tx = p->pos + timeStep / 2.f * p->vel;
			Vec3 tv = p->vel + timeStep / 2.f * p->oldforce / m_fMass;
			Vec3 nx = p->pos + timeStep * tv;
			Vec3 nv = p->vel + timeStep * p->force / m_fMass;

			p->vel = nv;
			p->pos = nx;
			p->oldforce = p->force;
		}
	}
	if (m_bFirstPrint)
	{
		m_bFirstPrint = false;
		for (int i = 0; i < m_iNumPoints; i++)
		{
			MPoint* p = &m_pPoints[i];
			cout << "Point " << i << " position: " << p->pos << " velocity: " << p->vel << endl;
		}
	}
	if (m_bCollision)
	{
		Vec3 min = Vec3(-0.5f, -0.5f, -0.5f);
		Vec3 max = Vec3(0.5f, 0.5f, 0.5f);
		for (int i = 0; i < m_iNumPoints; i++)
		{
			MPoint* p = &m_pPoints[i];
			if (p->fixed)
				continue;
			for (int j = 0; j < 3; j++)
			{
				float s = min.value[j];
				if (p->pos.value[j] >(max.value[j] - SPHERE_RADIUS))
				{
					s = max.value[j];
				}
				else if (p->pos.value[j] < (min.value[j] + SPHERE_RADIUS))
				{
					s = min.value[j];
				}
				else
				{
					continue;
				}
				float d = max.value[j] - min.value[j] - 2 * SPHERE_RADIUS;
				float n = fmodf(d / 2 + p->pos.value[j], 2 * d) - d / 2;
				if (n < d / 2)
				{
					n = (n + d / 2) * -1 - d / 2;
				}
				if (n > d / 2)
				{
					n = d - n;
				}
				p->vel[j] *= -1;
				p->pos[j] = n;
			}
		}
	}
}
void MassSpringSystemSimulator::onClick(int x, int y)
{
	m_trackmouse.x = x;
	m_trackmouse.y = y;
}
void MassSpringSystemSimulator::onMouse(int x, int y)
{
	m_oldtrackmouse.x = x;
	m_oldtrackmouse.y = y;
	m_trackmouse.x = x;
	m_trackmouse.y = y;
}
void MassSpringSystemSimulator::setMass(float mass)
{
	m_fMass = mass;
}
void MassSpringSystemSimulator::setStiffness(float stiffness)
{
	m_fStiffness = stiffness;
}
void MassSpringSystemSimulator::setDampingFactor(float damping)
{
	m_fDamping = damping;
}
int MassSpringSystemSimulator::addMassPoint(Vec3 position, Vec3 Velocity, bool isFixed)
{
	m_iNumPoints++;
	if (!m_pPoints)
	{
		m_pPoints = new MPoint[1];
	}
	else
	{
		MPoint* b = new MPoint[m_iNumPoints];
		memcpy(b, m_pPoints, sizeof(MPoint)*(m_iNumPoints - 1));
		delete[] m_pPoints;
		m_pPoints = b;
	}
	m_pPoints[m_iNumPoints - 1].pos = position;
	m_pPoints[m_iNumPoints - 1].vel = Velocity;
	m_pPoints[m_iNumPoints - 1].force = Vec3(0.f, 0.f, 0.f);
	m_pPoints[m_iNumPoints - 1].oldforce = Vec3(0.f, 0.f, 0.f);
	m_pPoints[m_iNumPoints - 1].fixed = isFixed;
	return m_iNumPoints - 1;
}
void MassSpringSystemSimulator::addSpring(int masspoint1, int masspoint2, float initialLength)
{
	m_iNumSprings++;
	if (!m_pSprings)
	{
		m_pSprings = new MSpring[1];
	}
	else
	{
		MSpring* b = new MSpring[m_iNumSprings];
		memcpy(b, m_pSprings, sizeof(MSpring)*(m_iNumSprings - 1));
		delete[] m_pSprings;
		m_pSprings = b;
	}
	m_pSprings[m_iNumSprings - 1].s1 = masspoint1;
	m_pSprings[m_iNumSprings - 1].s2 = masspoint2;
	m_pSprings[m_iNumSprings - 1].len = initialLength;
}
int MassSpringSystemSimulator::getNumberOfMassPoints()
{
	return m_iNumPoints;
}
int MassSpringSystemSimulator::getNumberOfSprings()
{
	return m_iNumSprings;
}
Vec3 MassSpringSystemSimulator::getPositionOfMassPoint(int index)
{
	if (index < 0 || index >= m_iNumPoints)
		return Vec3(0.f, 0.f, 0.f);
	return m_pPoints[index].pos;
}
Vec3 MassSpringSystemSimulator::getVelocityOfMassPoint(int index)
{
	if (index < 0 || index >= m_iNumPoints)
		return Vec3(0.f, 0.f, 0.f);
	return m_pPoints[index].vel;
}
void MassSpringSystemSimulator::applyExternalForce(Vec3 force)
{
	m_externalForce = force;
}
void MassSpringSystemSimulator::SetCollision(bool enable)
{
	m_bCollision = enable;
}