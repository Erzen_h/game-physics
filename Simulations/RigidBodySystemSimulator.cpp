#include "RigidBodySystemSimulator.h"
#include "MassSpringSystemSimulator.cpp" // UGLY
#include "collisionDetect.h"
#include "util/quaternion.h"
#include <algorithm>
#include <stdlib.h>


double getRand() {
	return ((double)rand()) / RAND_MAX * 10.f;
}

Mat4 Cuboid::localToWorldMat() {
	Mat4 scaleMat; scaleMat.initScaling(size.x, size.y, size.z);
	Mat4 rotMat = rot.getRotMat();
	Mat4 traMat; traMat.initTranslation(pos.x, pos.y, pos.z);
	return scaleMat * rotMat * traMat;
}

Vec3 Cuboid::localToWorld(Vec3& v) {
	return localToWorldMat().transformVector(v);
}

Vec3 Cuboid::worldToLocal(Vec3& v) {
	return localToWorldMat().inverse().transformVector(v);
}

Mat4 Cuboid::getCurrInvTensor() {
	auto tmp = rot.getRotMat();
	tmp.transpose();
	return tmp * invTensor * rot.getRotMat();
}

// returns vel in world space
// pos given in Worldspace
Vec3 Cuboid::getTotalVel(Vec3 pos) {
	if (fixed) {
		return Vec3(0.f);
	}
	pos = worldToLocal(pos);
	Vec3 angVel = getCurrInvTensor().transformVector(angMom);
	Vec3 velAtPos = cross(angVel, pos);
	return localToWorld(velAtPos) + vel;
}

RigidBodySystemSimulator::RigidBodySystemSimulator() {
	m_bGravity = false;
}

const char * RigidBodySystemSimulator::getTestCasesStr() {
	return "Demo 2 (Single Body),Demo 3 (2 Body),Demo 4";
}

void RigidBodySystemSimulator::initUI(DrawingUtilitiesClass * DUC) {
	this->DUC = DUC;

	TwAddVarRW(DUC->g_pTweakBar, "Damping", TW_TYPE_FLOAT, &m_fDamping, "min=0 max=1 step=0.01");

	switch (m_iTestCase) {
	case 0:
	case 1:
		m_bGravity = false;
		break;
	case 2:
		TwAddVarRW(DUC->g_pTweakBar, "Gravity", TW_TYPE_BOOLCPP, &m_bGravity, "");
		break;
	}
}

void RigidBodySystemSimulator::reset() {
	m_mouse.x = m_mouse.y = 0;
	m_trackmouse.x = m_trackmouse.y = 0;
	m_oldtrackmouse.x = m_oldtrackmouse.y = 0;
	m_externalForce = Vec3(0.f);
	m_bGravity = false;
	bodies.clear();
}
void RigidBodySystemSimulator::drawFrame(ID3D11DeviceContext* pd3dImmediateContext) {
	for (auto& c : bodies) {
		if (!c.render) continue;
		DUC->setUpLighting(Vec3(0, 0, 0), 0.4 * Vec3(1, 1, 1), 2000.0, Vec3(0.5, 0.5, 0.5));
		DUC->drawRigidBody(c.localToWorldMat());
	}
}
void RigidBodySystemSimulator::notifyCaseChanged(int testCase) {
	switch (testCase) {
	case 0: // single body
		reset();
		addRigidBody(Vec3(0, 0, 0), Vec3(1, 0.6, 0.5), 2);
		break;
	case 1: // 2 body
		reset();
		addRigidBody(Vec3(-1, -1, -1), Vec3(1, 0.6, 0.5), 2);
		addRigidBody(Vec3(1, 1, 1), Vec3(0.3, 0.3, 0.3), 1);
		setVelocityOf(0, Vec3(1, 1, 1));
		setOrientationOf(0, Quat(Normalize(Vec3(0, 1, 1)), 1.23));
		setVelocityOf(1, Vec3(-1, -1, -1));
		//applyForceOnBody(1, Vec3(0, 0.3, 0.3), Vec3(-1, -1, -1));
		break;
	case 2: // n body
		TwAddVarRW(DUC->g_pTweakBar, "Gravity", TW_TYPE_BOOLCPP, &m_bGravity, "");
		reset();
		
		// add single body for testing:
		//addRigidBody(Vec3(0, 0, 0), Vec3(1), 2);
		//setOrientationOf(0, Quat(Vec3(1, 0, 1), M_PI / 4) * Quat(Vec3(0,1,0), M_PI / 4));

		
		for (int i = -1; i < 2; ++++i) {
			for (int j = -1; j < 2; ++++j) {
				addRigidBody(Vec3(-2 * i, 3 + 2 + 2 * j, 0), Vec3(1), 2);
			}
		}
		for (int i = 0; i < 4; ++i) {
			Vec3 rand(getRand());
			setOrientationOf(i, Quat(Normalize(rand), getRand()));
			setVelocityOf(i, -bodies[i].pos * getRand() * 0.1);
		}

		// add floor
		addRigidBody(Vec3(0, -1001, 0), Vec3(2000, 2000, 2000), 200000); // floor
		bodies[bodies.size() - 1].fixed = true;
		bodies[bodies.size() - 1].render = false;
		
	}

}

void RigidBodySystemSimulator::externalForcesCalculations(float timeElapsed) {
	Point2D mouseDiff;
	mouseDiff.x = m_trackmouse.x - m_oldtrackmouse.x;
	mouseDiff.y = m_trackmouse.y - m_oldtrackmouse.y;
	if (mouseDiff.x != 0 || mouseDiff.y != 0)
	{
		Mat4 worldViewInv = Mat4(DUC->g_camera.GetWorldMatrix() * DUC->g_camera.GetViewMatrix());
		worldViewInv = worldViewInv.inverse();
		Vec3 inputView = Vec3((float)mouseDiff.x, (float)-mouseDiff.y, 0);
		Vec3 inputWorld = worldViewInv.transformVectorNormal(inputView);
		// find a proper scale!
		float inputScale = 0.0001f; // fix scale to take into account timeElapsed
		inputWorld = inputWorld * inputScale * timeElapsed;
		m_externalForce += inputWorld;
		cout << "Update external force:" << m_externalForce << endl;
	}
}

void RigidBodySystemSimulator::simulateTimestep(float timeStep) {
	for (int i = 0; i < bodies.size(); ++i) {
		auto& b = bodies[i];

		b.force += m_externalForce;
		Vec3 g = Vec3(0.f);
		if (m_bGravity) {
			g = Vec3(0, -9.81, 0);
		}

		auto angVel = getAngularVelocityOfRigidBody(i);
		if (!b.fixed) {
			b.pos += timeStep * b.vel;
			b.vel += timeStep * (b.force + b.mass * g) / b.mass;
			b.rot += (timeStep / 2) * Quat(angVel.x, angVel.y, angVel.z, 0) * b.rot;
			b.rot /= b.rot.norm();
			b.angMom += timeStep * b.torque;
		}

		// Collisions:

		struct Collision {
			int a;
			int b;
			CollisionInfo info;
			Vec3 relV;
		};

		std::vector<Collision> colls;
		
		int* numColls = new int[bodies.size()]();

		for (int j = i + 1; j < bodies.size(); ++j) {
			// Edge to Edge detection is pretty meh
			auto info = checkCollisionSAT(bodies[i].localToWorldMat(), bodies[j].localToWorldMat());
			if (!info.isValid) continue;
			auto p1 = bodies[i].worldToLocal(info.collisionPointWorld),
				p2 = bodies[j].worldToLocal(info.collisionPointWorld);
			auto relV = bodies[i].getTotalVel(p1) - bodies[j].getTotalVel(p2);

			auto normalB = bodies[j].rot.getRotMat().transformVector(info.normalWorld);

			relV = dot(relV, info.normalWorld) * info.normalWorld;

			std::cout << "Collision Normal (World Space): " << info.normalWorld << std::endl;
			std::cout << "Collision Normal (B Space): " << bodies[j].rot.getRotMat().transformVector(info.normalWorld) << std::endl;

			//auto relV = bodies[i].getTotalVel(info.collisionPointWorld) - bodies[j].getTotalVel(info.collisionPointWorld);
			// seperating 
			if (0 < dot(relV, bodies[j].rot.getRotMat().transformVector(info.normalWorld))) {
				std::cout << "Seperating" << std::endl;
				continue;
			}

			numColls[i]++;
			numColls[j]++;
			colls.push_back({ i, j, info, relV });
		}

		for (auto& c : colls) {
			auto& a = bodies[c.a];
			auto& b = bodies[c.b];
			auto relV = c.relV;
			auto info = c.info;

			float depth = info.depth / 2;
			if (a.fixed) {
				depth = info.depth;
			}
			if (b.fixed) {
				depth = info.depth;
			}
			if (!a.fixed) {
				a.pos += (depth / 2) * info.normalWorld;
			}
			if (!b.fixed) {
				b.pos -= (depth / 2) * info.normalWorld;
			}

			double impulse = -(1 + m_fDamping) * dot(relV, info.normalWorld);
			float invMassA = 1 / (a.mass / numColls[c.a]),
				invMassB = 1 / (b.mass / numColls[c.b]);
			Mat4 invTensorA = a.getCurrInvTensor(),
				invTensorB = b.getCurrInvTensor();
			if (a.fixed) {
				invMassA = 0;
				invTensorA = Mat4(0.f);
			}
			if (b.fixed) {
				invMassB = 0;
				invTensorB = Mat4(0.f);
			}
			impulse /= invMassA + invMassB
				 + dot(cross(invTensorA * cross(info.collisionPointWorld, info.normalWorld), info.collisionPointWorld)
					 + cross(invTensorB * cross(info.collisionPointWorld, info.normalWorld), info.collisionPointWorld),
					info.normalWorld);

			// assert(impulse >= 0);
			// lol...
			if (impulse < 0) impulse *= -1;

			if (!a.fixed) {
				a.vel += (impulse * info.normalWorld) / a.mass;
				a.angMom += cross(a.worldToLocal(info.collisionPointWorld), impulse * Normalize(a.worldToLocal(info.normalWorld)));
			}
			if (!b.fixed) {
				b.vel -= (impulse * info.normalWorld) / b.mass;
				b.angMom -= cross(b.worldToLocal(info.collisionPointWorld), impulse * Normalize(b.worldToLocal(info.normalWorld)));
			}
		}

		delete[] numColls;
	}
}


void RigidBodySystemSimulator::onClick(int x, int y) {
	m_trackmouse.x = x;
	m_trackmouse.y = y;
}
void RigidBodySystemSimulator::onMouse(int x, int y) {
	m_oldtrackmouse.x = m_trackmouse.x = x;
	m_oldtrackmouse.y = m_trackmouse.y = y;
}

int RigidBodySystemSimulator::getNumberOfRigidBodies() {
	return bodies.size();
}
Vec3 RigidBodySystemSimulator::getPositionOfRigidBody(int i) {
	return bodies[i].pos;
}
Vec3 RigidBodySystemSimulator::getLinearVelocityOfRigidBody(int i) {
	return bodies[i].vel;
}
Vec3 RigidBodySystemSimulator::getAngularVelocityOfRigidBody(int i) {
	return bodies[i].getCurrInvTensor() * bodies[i].angMom;
}

// TODO figure out if loc is world space or local
void RigidBodySystemSimulator::applyForceOnBody(int i, Vec3 loc, Vec3 force) {
	if (i < bodies.size()) {
		bodies[i].force += force;
		bodies[i].torque += cross(loc, force);
	}
}

void RigidBodySystemSimulator::addRigidBody(Vec3 position, Vec3 size, int mass) {
	Cuboid c = {};
	c.pos = position;
	c.size = size;
	c.mass = mass;

	// Geometric center = center of mass
	c.invTensor.value[0][0] = c.mass * (c.size.y * c.size.y + c.size.z * c.size.z) / 12;
	c.invTensor.value[1][1] = c.mass * (c.size.x * c.size.x + c.size.z * c.size.z) / 12;
	c.invTensor.value[2][2] = c.mass * (c.size.x * c.size.x + c.size.y * c.size.y) / 12;
	c.invTensor.value[3][3] = 1;
	c.invTensor = c.invTensor.inverse();
	bodies.push_back(c);
}
void RigidBodySystemSimulator::setOrientationOf(int i, Quat orientation) {
	if (i < bodies.size()) {
		orientation /= orientation.norm();
		bodies[i].rot = orientation;
	}
}
void RigidBodySystemSimulator::setVelocityOf(int i, Vec3 velocity) {
	if (i < bodies.size()) {
		bodies[i].vel = velocity;
	}
}
