#pragma once

#include "Simulator.h"
#include "util/quaternion.h"
#include <vector>
//add your header for your rigid body system, for e.g.,
//#include "rigidBodySystem.h" 

#define TESTCASEUSEDTORUNTEST 4

struct Force {
	Vec3 pos;
	Vec3 dir;
};

struct Cuboid {
	bool render = true;
	bool fixed = false;
	Vec3 pos; // center
	Vec3 prevPos;
	Vec3 size;
	Vec3 force;
	Vec3 torque;
	//std::vector<Force> forces;
	Vec3 vel = Vec3(0.f);
	Vec3 angMom = Vec3(0.f);
	Quat rot = Quat(Vec3(0, 1, 0), 0);
	Quat prevRot = Quat(Vec3(0, 1, 0), 0);
	Mat4 invTensor = Mat4(0.f);
	double mass;
	Vec3 worldToLocal(Vec3& v);
	Vec3 localToWorld(Vec3& v);
	Mat4 localToWorldMat();
	Vec3 getTotalVel(Vec3 pos);
	Mat4 getCurrInvTensor();
};

class RigidBodySystemSimulator: public Simulator
{
public:
	// Construtors
	RigidBodySystemSimulator();
	
	// Functions
	const char * getTestCasesStr();
	void initUI(DrawingUtilitiesClass * DUC);
	void reset();
	void drawFrame(ID3D11DeviceContext* pd3dImmediateContext);
	void notifyCaseChanged(int testCase);
	void externalForcesCalculations(float timeElapsed);
	void simulateTimestep(float timeStep);
	void onClick(int x, int y);
	void onMouse(int x, int y);

	// ExtraFunctions
	int getNumberOfRigidBodies();
	Vec3 getPositionOfRigidBody(int i);
	Vec3 getLinearVelocityOfRigidBody(int i);
	Vec3 getAngularVelocityOfRigidBody(int i);
	void applyForceOnBody(int i, Vec3 loc, Vec3 force);
	void addRigidBody(Vec3 position, Vec3 size, int mass);
	void setOrientationOf(int i,Quat orientation);
	void setVelocityOf(int i, Vec3 velocity);

private:
	float m_fDamping = 1;
	bool m_bGravity = false;
	// Attributes
	// add your RigidBodySystem data members, for e.g.,
	// RigidBodySystem * m_pRigidBodySystem;
	Vec3 m_externalForce = Vec3(0, 0, 0);

	std::vector<Cuboid> bodies;

	// UI Attributes
	Point2D m_mouse;
	Point2D m_trackmouse;
	Point2D m_oldtrackmouse;
};